﻿# OHStore-app

#### 介绍
OpenHarmony应用商店客户端，支持运行在Hi3516DV300及RK3568硬件平台

#### 软件架构
基于OpenHarmony3.2及以上版本开发的Js应用


#### 安装教程

1.  获取仓库代码
2.  使用DevEco Studio打开工程
3.  编译为hap安装包使用hdc工具安装到开发板

#### 效果展示

视频地址：[OH应用商店客户端演示](https://www.bilibili.com/video/BV1Cg4y1g7Ps/?pop_share=1&vd_source=92bd44c0785a6dc21aff0fccc2cb6441)

#### 使用说明

1. 首页查看应用商店已有app

![首页](screenshots/device/Screenshot_1660579264812.jpg)

2. 搜索tab页根据app名称查找相关app

![搜索](screenshots/device/Screenshot_1660579501872.jpg)

3. 分类tab页按类型查看app

![分类](screenshots/device/Screenshot_1660579520124.jpg)

点击查看全部按钮可查看对应分类下所有应用

![查看全部](screenshots/device/Screenshot_1660579529198.jpg)

4. 我的tab页查看当前登录用户信息，支持查看当前已安装应用/登录/注销登录/修改密码操作

![我的](screenshots/device/user_info.jpg)

![修改密码](screenshots/device/modify_password.jpg)

![已安装](screenshots/device/Screenshot_1660824771688.jpg)

5. 各tab页顶部可执行注册/登录操作

![注册](screenshots/device/Screenshot_1660145501395.jpg)

![登录](screenshots/device/Screenshot_1660145484045.jpg)

6. 点击app列表中app可进入对应详情页，执行安装/评论操作

![详情](screenshots/device/Screenshot_1660145624783.jpg)

![评论](screenshots/device/Screenshot_1663782468524.jpg)

﻿
#### OHStore-web
OH商店web端地址：[OH应用商店](http://app.oh.isrc.ac.cn/#/index)
