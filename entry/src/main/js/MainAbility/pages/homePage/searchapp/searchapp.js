/*
 * Copyright (c) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import http from '@ohos.net.http';
import router from '@system.router';
import bundle from '@ohos.bundle'
import util from '@ohos.util'
import prompt from '@system.prompt';
export default {
    data: {
        appList: [],
        appName: "",
        xtoken: "",
        xsession: "",
        accessToken: ""
    },
    onInit() {
    },
    onUser() {
        console.info("myapp page to user" + this.$app.$def.data.name);
        if (this.$app.$def.data.name == "guest") {
            router.push({
                uri: "pages/login/login",
                params: {
                    lastPage: "pages/index/index"
                }
            });
        } else {
            router.push({
                uri: "pages/userstate/userstate",
            });
        }
    },
    onDesc(appItem) {
        console.info("myapp page to desc, appname:" + appItem.name);
        console.info("myapp page to desc, downloadtext:" + appItem.version);
        // this page cannot get appItem.version, so need set appItem.version temporarily
        appItem.version = "test";
        router.push({
            uri: "pages/description/description",
            params: {
                appItem: appItem,
            }
        })
    },
    change(e) {
        this.appName = e.value;
    },
    submit() {
        this.get_xtoken();
    },
    get_xtoken() {
        let url  = this.$app.$def.data.usermgmtIP + '/v1/identity/verifycode-image'
        let httpRequest = http.createHttp();
        httpRequest.on('napi_headersReceive', (data) => {
        });
        httpRequest.request(
            url,
            {
                method: 'GET', // 可选，默认为“GET”
                header: {
                    'Content-Type': 'image/jpeg',
                },
                connectTimeout: 60000, // 可选，默认为60s
                readTimeout: 60000, // 可选，默认为60s
            },(err, data) => {
            if (!err) {
                this.xtoken = data.cookies.substring(data.cookies.length-130, data.cookies.length-94);
                this.xsession = data.cookies.substring(data.cookies.length-32);
                this.get_access_token();
            } else {
                //console.info('myapp2 get_xtoken err:' + err);
                httpRequest.destroy();
            }
        }
        );
    },
    get_access_token() {
        let url = this.$app.$def.data.usermgmtIP + '/v1/accesstoken';
        let httpRequest = http.createHttp();
        httpRequest.on('headersReceive', (data) => {
        });
        let session_value = 'AUTHSERVERSESSIONID=' + this.xsession + ';';
        let cookie_value = session_value + 'XSRF-TOKEN=' + this.xtoken + ';';
        httpRequest.request(
            url,
            {
                method: 'POST', // 可选，默认为“GET”
                header: {
                    'Content-Type': 'application/json',
                    'Cookie': cookie_value,
                    'Connection': 'keep-alive',
                    'Accept-Encoding': 'gzip, deflate, br',
                    'Accept': '*/*',
                    'X-XSRF-TOKEN': this.xtoken
                },
                extraData:
                {
                    "userFlag": "guest",
                    "password": "guest"
                },
                connectTimeout: 60000, // 可选，默认为60s
                readTimeout: 60000, // 可选，默认为60s
            },(err, data) => {
            if (!err) {
                //console.info('myapp get_access_token Result:' + data.result);
                console.info('myapp get_access_token code:' + data.responseCode);
                console.info('myapp get_access_token cookies:' + data.cookies);
                this.accessToken = JSON.parse(data.result).accessToken;
                console.info("myapp get_access_token accesstoken:" + this.accessToken);
                httpRequest.destroy();
                this.onSearchApp(this.appName);
            } else {
                console.info('myapp get_access_token error:' + err);
                httpRequest.destroy();
            }
        }
        );

    },
    onSearchApp(appName) {
        console.info("myapp onSearchApp");
        // 每一个httpRequest对应一个http请求任务，不可复用
        let httpRequest = http.createHttp();
        // 用于订阅http响应头，此接口会比request请求先返回。可以根据业务需要订阅此消息
        // 从API 8开始，使用on('headersReceive', Callback)替代on('headerReceive', AsyncCallback)。 8+
        httpRequest.on('headersReceive', (data) => {
            console.info('header: ' + data.header);
        });
        let session_value = 'AUTHSERVERSESSIONID=' + this.$app.$def.data.session + ';';
        let cookie_value = session_value + 'XSRF-TOKEN=' + this.$app.$def.data.token + ';';
        let url = this.$app.$def.data.appstoreIP + "/mec/appstore/v2/apps/action/query"
        httpRequest.request(
            url,
            {
                method: 'POST', // 可选，默认为“GET”
                header: {
                    'Content-Type': 'application/json',
                    'Cookie': cookie_value,
                    'Connection': 'keep-alive',
                    'Accept-Encoding': 'gzip, deflate, br',
                    'Accept': '*/*',
                    'X-XSRF-TOKEN': this.$app.$def.data.token,
                    'accesstoken': this.accessToken
                },
                extraData: {
                    "types": [],
                    "showType": ["public", "inner-public"],
                    "affinity": [],
                    "industry": [],
                    "workloadType": [],
                    "userId": "",
                    "queryCtrl": {
                        "limit": 15,
                        "offset": 0,
                        "sortItem": "createTime",
                        "sortType": "desc",
                        "status": ["Published"],
                        "appName": appName
                    }
                },
                connectTimeout: 60000, // 可选，默认为60s
                readTimeout: 60000, // 可选，默认为60s
            },(err, data) => {
            if (!err) {
                console.info('myapp Result:' + data.result);
                console.info('myapp query app code:' + data.responseCode);
                // data.header为http响应头，可根据业务需要进行解析
                //console.info('myapp header:' + data.header);
                //console.info('myapp cookies:' + data.cookies);
                if (JSON.parse(data.result).total == 0) {
                    prompt.showToast({
                        message: this.$t('Strings.search_null')
                    })
                    httpRequest.destroy();
                }
                this.appList = JSON.parse(data.result).results;

                for (var i = 0; i < this.appList.length; i++) {
                    this.onDownloadIcon(this.appList[i].packageId, i);
                    this.onAppComment(this.appList[i].appId, i);
                    this.onAppScore(this.appList[i].appId, i);
                }
                console.info("myapp results length:" + this.appList.length);
            } else {
                console.info('myapp error:' + err);
                // 当该请求使用完毕时，调用destroy方法主动销毁。
                httpRequest.destroy();
            }
        }
        );
    },
    onDownloadIcon(packageId, i) {
        console.info("myapp onDownloadIcon");
        // 每一个httpRequest对应一个http请求任务，不可复用
        let httpRequest = http.createHttp();
        // 用于订阅http响应头，此接口会比request请求先返回。可以根据业务需要订阅此消息
        // 从API 8开始，使用on('headersReceive', Callback)替代on('headerReceive', AsyncCallback)。 8+
        httpRequest.on('headersReceive', (data) => {
            console.info('header: ' + data.header);
        });
        let url = this.$app.$def.data.appstoreIP + "/mec/appstore/v1/packages/" + packageId + "/action/download-icon"
        httpRequest.request(
        // 填写http请求的url地址，可以带参数也可以不带参数。URL地址需要开发者自定义。GET请求的参数可以在extraData中指定
            url,
            {
                method: 'GET', // 可选，默认为“GET”
                // 开发者根据自身业务需要添加header字段
                header: {
                    'Content-Type': 'application/json'
                },
                // 当使用POST请求时此字段用于传递内容

                extraData: {
                    //"data": "data to send",
                },
                connectTimeout: 60000, // 可选，默认为60s
                readTimeout: 60000, // 可选，默认为60s
            },(err, data) => {
            if (!err) {
                // data.result为http响应内容，可根据业务需要进行解析
                console.info('myapp Result:' + data.result);
                console.info('myapp request icon code:' + data.responseCode);
                // data.header为http响应头，可根据业务需要进行解析
                var lresult = new Uint8Array(data.result);
                var that = new util.Base64();
                var finresult = that.encodeToStringSync(lresult);
                var pic = "data:image/jpeg;base64," + finresult;
                this.appList[i].deployMode = pic;
            } else {
                console.info('myapp error:' + err);
                // 当该请求使用完毕时，调用destroy方法主动销毁。
                httpRequest.destroy();
            }
        }
        );
    },
    onAppScore(appId, i) {
        console.info("myapp onAppScore");
        // 每一个httpRequest对应一个http请求任务，不可复用
        let httpRequest = http.createHttp();
        // 用于订阅http响应头，此接口会比request请求先返回。可以根据业务需要订阅此消息
        // 从API 8开始，使用on('headersReceive', Callback)替代on('headerReceive', AsyncCallback)。 8+
        httpRequest.on('headersReceive', (data) => {
            console.info('header: ' + data.header);
        });
        let url = this.$app.$def.data.appstoreIP + "/mec/appstore/v1/apps/" + appId
        httpRequest.request(
            // 填写http请求的url地址，可以带参数也可以不带参数。URL地址需要开发者自定义。GET请求的参数可以在extraData中指定
            url,
            {
                method: 'GET', // 可选，默认为“GET”
                // 开发者根据自身业务需要添加header字段
                header: {
                    'Content-Type': 'application/json',
                    'accesstoken': this.accessToken
                },
                // 当使用POST请求时此字段用于传递内容

                extraData: {
                    //"data": "data to send",
                },
                connectTimeout: 60000, // 可选，默认为60s
                readTimeout: 60000, // 可选，默认为60s
            },(err, data) => {
            if (!err) {
                // data.result为http响应内容，可根据业务需要进行解析
                this.appList[i].atpTestTaskId = JSON.parse(data.result).score;
                console.info('myapp request onAppScore code:' + this.appList[i].atpTestTaskId);
            } else {
                console.info('myapp error:' + err);
                // 当该请求使用完毕时，调用destroy方法主动销毁。
                httpRequest.destroy();
            }
        }
        );
    },
    onAppComment(appId, i) {
        console.info("myapp onAppComment");
        // 每一个httpRequest对应一个http请求任务，不可复用
        let httpRequest = http.createHttp();
        // 用于订阅http响应头，此接口会比request请求先返回。可以根据业务需要订阅此消息
        // 从API 8开始，使用on('headersReceive', Callback)替代on('headerReceive', AsyncCallback)。 8+
        httpRequest.on('headersReceive', (data) => {
            console.info('header: ' + data.header);
        });
        let url = this.$app.$def.data.appstoreIP + "/mec/appstore/v2/apps/" + appId + "/comments?limit=100&offset=0";
        httpRequest.request(
            // 填写http请求的url地址，可以带参数也可以不带参数。URL地址需要开发者自定义。GET请求的参数可以在extraData中指定
            url,
            {
                method: 'GET', // 可选，默认为“GET”
                // 开发者根据自身业务需要添加header字段
                header: {
                    'Content-Type': 'application/json',
                    'accesstoken': this.accessToken
                },
                // 当使用POST请求时此字段用于传递内容

                extraData: {
                    //"data": "data to send",
                },
                connectTimeout: 60000, // 可选，默认为60s
                readTimeout: 60000, // 可选，默认为60s
            },(err, data) => {
            if (!err) {
                // data.result为http响应内容，可根据业务需要进行解析
                this.appList[i].contact = JSON.parse(data.result).total;
            } else {
                console.info('myapp error:' + err);
                // 当该请求使用完毕时，调用destroy方法主动销毁。
                httpRequest.destroy();
            }
        }
        );
    }
}
