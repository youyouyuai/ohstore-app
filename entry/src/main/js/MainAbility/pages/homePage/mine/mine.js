/*
 * Copyright (c) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import http from '@ohos.net.http';
import router from '@system.router';
import bundle from '@ohos.bundle'
import util from '@ohos.util'
import prompt from '@system.prompt';
export default {
    data: {
        userInfo: {
            username: "guest",
            createTime: "登陆后查看",
            mailAddress: "登陆后查看",
        },
        buttonText: "登录"
    },
    onInit() {
        let session_value = 'AUTHSERVERSESSIONID=' + this.$app.$def.data.session + ';';
        let cookie_value = session_value + 'XSRF-TOKEN=' + this.$app.$def.data.token + ';';
        let url  = this.$app.$def.data.usermgmtIP + '/auth/login-info'
        let httpRequest = http.createHttp();
        httpRequest.on('napi_headersReceive', (data) => {
        });
        httpRequest.request(
            url,
            {
                method: 'GET', // 可选，默认为“GET”
                header: {
                    'Cookie': cookie_value,
                },
                connectTimeout: 60000, // 可选，默认为60s
                readTimeout: 60000, // 可选，默认为60s
            },(err, data) => {
            if (!err) {
                if (this.$app.$def.data.name != "guest") {
                    this.userInfo = JSON.parse(data.result);
                    this.buttonText = "注销登录"
                }

            } else {
                httpRequest.destroy();
            }
        }
        );
    },
    onUser() {
        if (this.$app.$def.data.name == "guest") {
            router.push({
                uri: "pages/login/login",
                params: {
                    lastPage: "pages/index/index"
                }
            });
        } else {
            let session_value = 'AUTHSERVERSESSIONID=' + this.$app.$def.data.session + ';';
            let cookie_value = session_value + 'XSRF-TOKEN=' + this.$app.$def.data.token + ';';
            let url  = this.$app.$def.data.usermgmtIP + '/auth/logout'
            let httpRequest = http.createHttp();
            httpRequest.on('napi_headersReceive', (data) => {
            });
            httpRequest.request(
                url,
                {
                    method: 'GET', // 可选，默认为“GET”
                    header: {
                        'Cookie': cookie_value,
                    },
                    connectTimeout: 60000, // 可选，默认为60s
                    readTimeout: 60000, // 可选，默认为60s
                },(err, data) => {
                if (!err) {
                    this.$app.$def.data.name = "guest"
                    router.push({
                        uri: "pages/index/index",
                    });
                } else {
                    httpRequest.destroy();
                }
            }
            );

        }
    },

}
