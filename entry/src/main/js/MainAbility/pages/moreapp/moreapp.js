/*
 * Copyright (c) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import http from '@ohos.net.http';
import router from '@system.router';
import bundle from '@ohos.bundle'
import util from '@ohos.util'
import prompt from '@system.prompt';
export default {
    data: {
        typeData: ['视频应用', '安全', '区块链', '物联网', '大数据', 'AR/VR', '游戏', '其他'],
        typeName: ["Video Application", "Safety", "Blockchain", "Internet of Things", "Big Data", "AR/VR", "Game", "Others"],
        xtoken: "",
        xsession: "",
        accessToken: "",
    },
    onInit() {
    },
    onDesc(appItem) {
        console.info("myapp2 page to desc, appname:" + appItem.name);
        appItem.version = "test";
        router.push({
            uri: "pages/description/description",
            params: {
                appItem: appItem,
            }
        })
    },

}
